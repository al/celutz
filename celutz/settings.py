"""
Django settings for celutz project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

import altitude.providers

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'ei3#@ejlp((&tlx2jrscs^wrvpn$y4o-7_(-$a_uc9%j3eux1*'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '::1']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'panorama.apps.PanoramaConfig',
    'altitude.apps.AltitudeConfig',
    'api'
)

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'celutz.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.csrf',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'celutz.wsgi.application'

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'fr'

LANGUAGES = [
    ('en', 'English'),
    ('fr', 'French'),
    ('es', 'Spanish'),
]

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_ROOT = os.path.join(BASE_DIR, 'static')
STATIC_URL = '/static/'


# Is it required to login to use celutz?
LOGIN_REQUIRED = False

# Altitude providers are tried in order until obtaining a result.
ALTITUDE_PROVIDERS = [altitude.providers.GeoportailProvider,
                      altitude.providers.GeonamesProvider]
# Connection timeout for each provider, in seconds
ALTITUDE_PROVIDER_TIMEOUT = 3.

# For uploaded panorama
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

# Relative to MEDIA_ROOT and MEDIA_URL
PANORAMA_TILES_DIR = "tiles"

# Maximum distance for reference points to be displayed when viewing a panorama
# (in meters)
PANORAMA_MAX_DISTANCE = 50000

# When locating a reference point, only consider panoramas that are closer than
# this maximum distance, in meters.
# Cannot be greater than PANORAMA_MAX_DISTANCE.
LOCATE_POINT_MAX_DISTANCE = 50000

# Map bounds (in degrees) for the main view.
# If not defined, all points are used to fit the view.
#MAP_BOUNDS = {
#  "min_lat": 45.17,
#  "max_lat": 45.20,
#  "min_lon": 5.68,
#  "max_lon": 5.77,
#}

# Celery configuration
CELERY_BROKER_URL = 'redis://'
CELERY_RESULT_BACKEND = 'redis://'
CELERY_TASK_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']

# Local settings overriding
try:
    from celutz.local_settings import *
except ImportError:
    pass

# Ensure that LOCATE_POINT_MAX_DISTANCE <= PANORAMA_MAX_DISTANCE
LOCATE_POINT_MAX_DISTANCE = min(LOCATE_POINT_MAX_DISTANCE, PANORAMA_MAX_DISTANCE)
