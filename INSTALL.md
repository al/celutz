Installing celutz
=================

System requirements
-------------------

- Python version 3 (at least 3.7)
- At least 10 GB of disk space.  You should provision 100 MB for each panorama:
  panoramic pictures can be pretty big to start with, and celutz generates
  a lot of tiles during the initial upload.
- At least 1.5G of RAM, mainly for tile generation.  If you generate tiles on
  another computer, then you need much less, probably as little as 512 MB of
  RAM.  Celutz is a quite simple Django application.
- a fast CPU, again for tile generation.  Having multiple CPU will not be useful
  in most cases, because tile generation is single-threaded.

For reference, generating tiles for a quite large panorama (30000x3487 pixels,
amounting to 105 Mpixels, storing in JPEG) took 855 MB of RAM using Python 3.5
with Pillow 3.0.0 on a x86_64 Linux system.

Installation
------------

Celutz is a fairly standard Django application: refer to the Django
documentation for deployment methods.  The initial installation for development
should look like this:

    apt install build-essential python3-venv python3-dev libjpeg-dev libtiff5-dev zlib1g-dev libopenjp2-7-dev redis sqlite3 libsqlite3-dev
    python3 -m venv ~/mycelutzvenv
    . ~/mycelutzvenv/bin/activate
    pip install --upgrade pip
    pip install "setuptools<58.0.0"
    pip install -r requirements.txt

Redis is used as a message broker between Django and Celery workers, used for tile generation (see below).

Configuration
-------------

To configure the application, don't edit `celutz/settings.py` directly, but
instead create a file `celutz/local_settings.py` with your local modifications.

Some Django things you should really configure:

- `SECRET_KEY`
- `ALLOWED_HOSTS`
- `DEBUG`
- database configuration

There are also celutz-specific settings:

- `LOGIN_REQUIRED`: is celutz public or are users required to login first?
- `PANORAMA_TILES_DIR`: where the tiles are, relatively to `MEDIA_ROOT` and `MEDIA_URL`
- `PANORAMA_MAX_DISTANCE`: maximum distance (in meters) for reference points to be displayed when viewing a panorama
- `LOCATE_POINT_MAX_DISTANCE`:  when locating a reference point, only consider panoramas that are closer than this maximum distance, in meters.
- `MAP_BOUNDS`: if defined, limit the view of the main map to the given rectangle.  By default, the view is fitted to display all points.  See `celutz/settings.py` for an example.

Then run the migrations:

    python manage.py migrate

And create a superuser:

    python manage.py createsuperuser

Lastly, you should collect static files to serve them:

    python manage.py collectstatic

Development
-----------

Just run the builtin Django server:

    ./manage.py runserver

Alternatively, you can use gunicorn exactly like in production.

You also need to launch a celery worker (see below).  Redis is needed to
server as a message broker between Django and Celery workers.  By default
on Debian, the Redis server is available on localhost without authentication,
which means that no specific configuration is required.

Production
----------

To run the WSGI server, for instance gunicorn:

    gunicorn celutz.wsgi:application

Internally, `dj_static` is used to serve static and media files (including tiles).

As an alternative, you may wish to serve media files (everything in `media/`)
with your web server directly.

You probably also need to configure your webserver to allow to send very large
files in a POST request.  An upper limit of 200 MB should be enough, even for
very large pictures in raw format.

Tile generation with Celery
---------------------------

Tile generation uses Celery, because it is quite a heavy task CPU-wise.

To launch a celery worker, run this in your virtualenv:

    celery -A celutz.celery worker --loglevel=info -c 1

This tells celery to handle at most one task at a time: `-c 1`.  Indeed,
generating tiles for a single panorama can take quite a lot of RAM.
If you have enough RAM (2GB+) and multiple CPU, you can increase this
parameter to generate tiles for multiple panoramas in parallel.

By default, we configure Celery to use Redis as a message queue.  This is
of course configurable if you have specific requirements.

Example production configuration with Nginx and Systemd
-------------------------------------------------------

Example systemd service files, to be installed in `/etc/systemd/system`:

- `example_config/celutz.service`: main Django app running with gunicorn
- `example_config/celutz-worker.service`: celery worker, used to generate tiles

You probably need to adapt paths, users, etc.

The following Nginx configuration can be used as a template:

- `example_config/nginx.conf`

Updating a production installation
----------------------------------

There is a script, `update_prod.sh`, that handles updating an existing
production installation.  It install new dependencies and collect static
files.

Importing reference points
--------------------------

Gathering reference points can be a bit of a hassle.

Some reference points are provided alongside celutz, in `panorama/fixtures`.
To import one set of reference points into the database, run:

    python manage.py loaddata refpoints_mycity

Upgrading from the PHP version of celutz
----------------------------------------

Were you previously using the PHP version of celutz?  You can import all your
old data!  See `UPGRADE.md`.
