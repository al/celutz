��    N      �  k   �      �     �  "   �     �     �     �  	   �     �     �  >        D     ^     f     o  
   v     �  
   �  	   �     �     �  	   �  >   �                          /  
   E     P     T     f     s     �     �  	   �     �  #   �  
   �     	     	     =	  E   Y	     �	     �	  +   �	     �	     �	     
     4
     ;
  +   L
     x
     ~
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
            	     	     
   (     3     8     >  	   G  	   Q     [     k  
   |  
   �     �     �  K  �       2     	   H     R     Z  
   `     k     q  G   y  %   �  
   �  
   �  	   �            
   (  
   3     >     G  
   N  B   Y     �     �  	   �     �     �     �     �     �            !   (     J  	   S     ]  $   w     �     �  #   �  #   �  H        X     g  8   |     �     �  #   �  
          %   ,     R     Y     b  
   r     }     �     �     �     �     �     �     �     �  
      	             !     %     +  	   4     >     J     _     u  
   �  
   �     �     <         7      ;   
          L                    .      9      1   3          8          K   "   4   (         5                              /      M      #   *      :   $   F   H             D       I              N   -          !               C           6       0   =   E                   )      ?       ,           '       B   @   G   +       %   &       	   A      >   2      J        360° panorama A panorama can't reference itself. About Add Admin Altitude: Bearing: Cancel Celutz is a tool for managing and referencing panoramic photos Celutz, a panorama viewer Control Controls Delete Elevation: Enter an address In degrees In meters Insert Language Latitude: Launched tiles regeneration, it may take some time to complete Legend Localized point Locate Locate GPS point Locate existing point Longitude: Map Name of the point New panorama New reference point No panorama seeing this point ! Panorama Panoramas Panoramas seeing the point Panoramas seeing the searched point Parameters Point of interest (other) Point of interest (subscriber) Point of interest (waiting) Position {xy} is outside the bounds of the image ({width}, {height}). Project homepage Reference point Regenerate tiles for the selected panoramas Result of research Results for the point Results for the searched point Search Tiles available? Whether the panorama loops around the edges Zoom: altitude altitude at ground level altitude: at ground altitude: height above ground height above ground: image image height image width kind latitude latitude: longitude longitude: name other panorama panoramas reference reference point reference points references subscriber waiting {refpoint} at {xy} in {pano} Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-01 20:50+0000
PO-Revision-Date: 2018-07-01 22:56+0200
Last-Translator: 
Language-Team: 
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
 Panorama de 360° Un panorama ne peut pas se référencer lui-même. À propos Ajouter Admin Altitude : Cap : Annuler Celutz est un outil pour gérer et référencer des photos panoramiques Celutz, un visualisateur de panoramas Contrôles Contrôles Supprimer Élévation : Entrer une adresse En degrés En mètres Insérer Langue Latitude : Regénération des tuiles en cours, cela pourrait prendre du temps Légende Point localisé Localiser Localiser par GPS Localiser un point Longitude : Carte Nom du point Ajouter un panorama Ajouter un point Aucun panorama ne voit ce point ! Panorama Panoramas Panoramas voyant le point Panoromas voyant le point recherché Paramètres Point d&#x27;intérêt (autre) Point d&#x27;intérêt (abonné·e) Point d&#x27;intérêt (en attente) La position {xy} est en dehors de l'image de taille ({width}, {height}). Page du projet Point de référence Regénérer les tuiles pour les panoramas sélectionnés Résultat de la recherche Résultats pour le point Résultats pour le point recherché Rechercher Tuiles disponibles ? Si le panorama boucle sur ses côtés Zoom : altitude altitude au sol altitude : à altitude au sol : hauteur au-dessus du sol hauteur au-dessus du sol : image hauteur de l'image largeur de l'image type latitude latitude : longitude longitude : nom autre panorama panoramas référence point de référence points de référence références abonné·e en attente {refpoint} en {xy} sur {pano} 